import Vue from 'vue'
import VueRouter from 'vue-router'
// import LatestMovie from '@/components/LatestMovie'
// import Movie from '@/components/Movie'
// import SearchMovie from '@/components/SearchMovie'
import Home from '@/components/Home'
import Login from '@/components/Login'
import Register from '@/components/Register'
import ApproveCourse from '@/components/ApproveCourse'
import Doccument from '@/components/Doccument'
import AddVideo from '@/components/AddVideo'
import AddDoc from '@/components/AddDoc'
import EditVideo from '@/components/EditVideo'
import EditDoc from '@/components/EditDoc'
import EditIFDoc from '@/components/EditIFDoc'
import EditIFVideo from '@/components/EditIFVideo'
import SearchQuiz from '../components/SearchQuiz'
import DetailQuiz from '../components/DetailQuiz'
import HistoryQuiz from '../components/HistoryQuiz'
import CreateQuiz from '../components/CreateQuiz'
import CreateCourse from '../components/CreateCourse'
import MyCourse from '../components/MyCourse'
import DetailCourse from '../components/DetailCourse'
Vue.use(VueRouter)

export default new VueRouter({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/approveCouse',
      name: 'ApproveCourse',
      component: ApproveCourse
    },
    {
      path: '/doccument',
      name: 'Doccument',
      component: Doccument
    },
    {
      path: '/addvideo',
      name: 'AddVideo',
      component: AddVideo
    },
    {
      path: '/adddoc',
      name: 'AddDoc',
      component: AddDoc
    },
    {
      path: '/search-quiz',
      name: 'Search Quiz',
      component: SearchQuiz
    },
    {
      path: '/detail-quiz',
      name: 'Detail Quiz',
      component: DetailQuiz
    },
    {
      path: '/history-quiz',
      name: 'History Quiz',
      component: HistoryQuiz
    },
    {
      path: '/create-quiz',
      name: 'Create Quiz',
      component: CreateQuiz
    },
    {
      path: '/editvideo',
      name: 'EditVideo',
      component: EditVideo
    },
    {
      path: '/editdoc',
      name: 'EditDoc',
      component: EditDoc
    },
    {
      path: '/editvideo/info',
      name: 'EditIFVideo',
      component: EditIFVideo
    },
    {
      path: '/editdoc/info',
      name: 'EditIFDoc',
      component: EditIFDoc
    },
    {
      path: '/create-course',
      name: 'Create Course',
      component: CreateCourse
    },
    {
      path: '/my-course',
      name: 'My Course',
      component: MyCourse
    },
    {
      path: '/detail-course/:id',
      name: 'Detail Course',
      component: DetailCourse,
      props: true
    }
  ],
  mode: 'history'
})
